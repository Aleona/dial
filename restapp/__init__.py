from flask import Flask
from config import Config

restapp = Flask(__name__)
restapp.config.from_object(Config)

from restapp import routes