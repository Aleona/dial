from flask import render_template
from restapp import restapp

@restapp.route('/')
@restapp.route('/index')
def index():
    user = {'username': 'Aldi'}
    return render_template('index.html', title='Домашняя', user=user)